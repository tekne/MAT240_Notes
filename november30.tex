\documentclass{article}
\title{MAT240 Notes: Determinants}
\author{Jad Elkhaleq Ghalayini}
\date{November 30 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}{Proposition}
\newtheorem{lemma}{Lemma}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\begin{document}
\maketitle
\[\det(A) = \sum_\sigma sign(\sigma)A_{\sigma(1)1}...A_{\sigma(n)n}, A \in M_{n \times n}(F)\]
Properties of determinants
\begin{itemize}
    \item If \(A\) is upper/lower triangular, \[\det(A) = \prod^n_{k = 0}A_{kk}\]
    \item \(\det(A) = \det(A^T)\)
\end{itemize}
\begin{theorem}
(Row and column operations). Let \(A \in M_{n \times n}(F)\). Suppose \(A'\) is obtained from \(A\) by
\begin{itemize}
    \item [C1] Interchanging two columns, then \(\det(A') = -\det(A)\)
    \item [C2] Multiplying a column by a scalar \(\lambda\) (even if zero), then \(\det(A') = \lambda\det(A)\)
    \item [C3] Adding a multiple of a column to another column, then \(\det(A') = \det(A)\)
\end{itemize}
Likewise for row operations (R1, R2, R3)
\end{theorem}
\begin{proof}
Properties for C2, C3 follow because the determinant is multilinear in the columns, and since the determinant is zero whenever two columns coincide. C1 follows from the fact that the determinant is zero when the two columns coincide, meaning that switching two columns negates the determinant.

This is the same for row operations because \(\det(A^T) = \det(A)\), allowing us to switch the rows and columns, perform column operations and switch back.
\end{proof}
Example: \[F = \mathbb{Z}_3, \begin{vmatrix}2 & 0 & 1 & 2 & 0 \\ 2 & 1 & 2 & 0 & 2 \\ 1 & 1 & 0 & 2 & 1 \\ 0 & 0 & 1 & 0 & 2 \\ 2 & 2 & 2 & 2 & 1\end{vmatrix}\]
We perform row and column operations to obtain
\[ = \begin{vmatrix}2 & 0 & 1 & 2 & 0 \\ 0 & 1 & 1 & 1 & 2 \\ 0 & 1 & 1 & 1 & 1 \\ 0 & 0 & 1 & 0 & 2 \\ 0 & 2 & 1 & 0 & 1\end{vmatrix} = \begin{vmatrix}2 & 0 & 1 & 2 & 0 \\ 0 & 1 & 1 & 1 & 2 \\ 0 & 0 & 0 & 0 & 2 \\ 0 & 0 & 1 & 0 & 2 \\ 0 & 0 & 2 & 1 & 0\end{vmatrix} = \begin{vmatrix}2 & 0 & 2 & 1 & 0 \\ 0 & 1 & 1 & 1 & 2 \\ 0 & 0 & 1 & 2 & 0 \\ 0 & 0 & 0 & 1 & 2 \\ 0 & 0 & 0 & 0 & 2\end{vmatrix} = 2 \cdot 2 = 4 = 1\]
\begin{theorem}
Let \(A \in M_{n \times n}(F)\). 
\begin{itemize}
    \item [Then] \(\det(A) \neq 0\)
    \item [\(\iff\)] \(rank(A) = n\)
    \item [\(\iff\)] columns are linearly independent
    \item [\(\iff\)] rows are linearly independent
    \item [\(\iff\)] \(A\) is invertible
\end{itemize}
\end{theorem}
\begin{proof}
Note that whenever \(A'\) is obtained from \(A\) by row operations \(R_1, R_2, R_3\), \(\det(A) = a\det(A')\) for some \(a \in F: a \neq 0\). Using row operations, we can bring \(A\) to row reduced echelon form, \(\det(A) \neq 0 \iff \det(A') \neq 0\).
If \(rank(A') = rank(A) < n\), then \(A'\) has a zero row, so \(\det(A) = 0\). If \(rank(A') = n\), then \(A' = I\) so \(\det(A') = 1 \implies \det(A) \neq 0\).
\end{proof}
\begin{theorem}
If \(A, B \in M_{n \times n}(F)\), then \(\det(AB) = \det(A)\det(B)\)
\end{theorem}
\begin{proof}
If \(\det(A) = 0\), then \(A\) is not invertible, so \(AB\) is not invertible (as \(rank(AB) \leq rank(A)\), which implies \(\det(AB) = 0\).

Assume \(\det(A) \neq 0\). Consider the multilinear functional 
\[\phi: F^n \times ... \times F^n \to F,(w_1,...,w_n) \mapsto \frac{\det(Aw_1,...,Aw_n)}{\det(A)}\] 
\(\phi(w_1,...,w_n) = 0\) if \(w_i = w_j\) for some \(i \neq j\).
\[\phi(e_1,...,e_n) = \frac{\det(Ae_1,...,Ae_n)}{\det(A)} = \frac{\det(A)}{\det(A)} = 1\]
Hence, by the theorem from last time, \(\phi(w_1,...,w_n) = \det(w_1,...w_n)\).
Take \(w_1 = Be_1,...,w_n = Be_n\). Then \[\phi(w_1,...,w_n) = \det(Be_1,...,Be_n) = \det(B) = \frac{\det(ABe_1,...,ABe_n)}{\det(A)}\]\[ = \frac{\det(AB)}{\det(A)} \implies \det(AB) = \det(A)\det(B)\]
\end{proof}

Another property:
If \(A\) has a block upper triangular form,
\[A = \begin{pmatrix} A' & * \\ 0 & A''\end{pmatrix}\]
then \(\det(A) = \det(A')\det(A'')\).
\end{document}