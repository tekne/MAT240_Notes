\documentclass{article}
\title{MAT240 Notes: Basis and Dimension}
\author{Jad Elkhaleq Ghalayini}
\date{October 10 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{fact}{Fact}
\begin{document}
\maketitle
 
\section{Review}

\(V\) is a vector space over field \(F\). Recall \(S \subseteq V\) is \underline{linearly independent} if for all distinct \(v_1,...,v_k \in S\) the equation \(a_1v_1 + ... + a_kv_k = 0\) only has the trivial solution \(a_1 = ... = a_k = 0\). The set may be infinite, but linear combinations are always finite.

\subsection{Basic Observations}
\begin{enumerate}
\item \(S\) is linearly dependent \(\iff\) \(\exists v \in S : v \in span(S \setminus \{v\})\).
\item \(S\) is linearly independent \(\implies\) every subset of \(S\) is linearly independent
\item \(S\) is linearly dependent \(\implies\) every subset of \(V\) containing \(S\) is linearly dependent
\item If \(S\) is linearly independent, and \(v \in V \setminus S\), then \(S \cup \{v\}\) is linearly independent \(\iff v \notin span(S)\).
\begin{proof}
We show \(S \cup \{v\}\) is linearly dependent \(\iff v \in span(S)\). Suppose \(v \in span(S)\). Then by (1), \(S \cup \{v\} = S'\) is linearly dependent.

For the other direction, assume \(S \cup \{v\}\) is linearly dependent, by definition \(\exists v_1, ...,  v_k \in S : bv + a_1v_1 + ... + a_kv_k = 0\) has a nontrivial solution. Therefore, \(bv = -a_1v_1 - ... - a_kv_k\). \(b \neq 0\) since otherwise \(v_1,...,v_k\) would be linearly dependent. Therefore, dividng both sides by \(b\), \(v = -\frac{a_1}{b}v_1 - ... - \frac{a_k}{b}v_k \implies v \in span(S)\).
\end{proof}
\end{enumerate}

\section{Basis}
\begin{definition}
A subset \(\beta \subseteq V\) is a \underline{basis of \(V\)} if
\begin{itemize}
\item \(span(\beta) = V\)
\item \(\beta\) is linearly independent
\end{itemize}

If \(V = \{0\}\), \(\beta = \varnothing\) is considered a basis.
\end{definition}

Examples:
\begin{enumerate}
\item \(V = F^n\) has standard basis \(\beta = \{v_1,...,v_n\}\) where \(v_1 = (1,0,...,0), v_2 = (0,1,0,...,0), ...\). It also has basis \(w_1 = (1,0,...,0), w_2 = (1,1,0,...,0), ...\).
\item \(V = Pol_n(\mathbb{R})\) has basis \(\beta = \{p_0, p_1, ..., p_n\}\) where \(p_0(x) = 1, p_1(x) = x, p_2(x) = x^2, ...\).
\item \(M_{m \times n}(F)\) has a standard basis consisting of all possible matrices in the subset of the form where one entry is one, and all others are 0 (there being \(mn\) of these). This is basically the same example as the first one, but rearranged as a matrix.
\item \(F^{\infty}_{fin}\) - the infinite sequences of finite length - has a standard basis given by \(v_1 = (1,0,0,...), v_2= (0,1,0,...), ...\) and so on. Note that a linear combination of such vectors is always a finite sequence, as we are only allowed finite linear combinations.
\item \(F^{\infty}\), the space of all sequences, doesn't have an "obvious" basis. The example above would not be a basis, as finite linear combinations would only give sequences of finite length, e.g. \((1,1,1,1,1,...)\) cannot be a linear combination of the above vectors.
\item \(V = \mathcal{F}(\mathbb{R},\mathbb{R})\) also has no obvious basis.	
\end{enumerate}

\begin{theorem}
A subset \(\beta \subseteq V\) is a basis \(\iff\) every \(v \in V\) can be uniquely written as a linear combination of elements of \(\beta\).
\end{theorem}

\begin{proof}
Starting from the right to the left, suppose every \(v \in V\) is a unique linear combination (up to re-ordering/adding zeroes) of elements of \(\beta \implies span(\beta) = V\). Since every vector corresponds to only one combination, 0 must correspond to only one combination. Since the trivial combination \(0v_1 + ... + 0v_k\) always equals 0, this means \(a_1v_1 + ... + a_kv_k = 0\) only has the trivial solution, e.g. \(\beta\) is linearly independent, therefore it is, by definition, a basis for \(V\).

For the other direction, assume \(\beta\) is a basis, let \(v \in V\). Since \(span(\beta) = V\), we can write \(v = a_1v_1 +...+ a_kv_k\) with \(v_1,...,v_k \in \beta\). Suppose also \(v = a_1'v_1' + ... + a_k'v_l'\) with \(v_1',...,v_l' \in \beta\). We may assume \(k = l, v_1 = v_1', ..., v_k = v_k'\) (if not add zeroes, re-order). So then since \(v - v = 0 = (a_1 - a_1')v_1 + ... + (a_k - a_k')v_k\), all the coefficients \(a_1 - a_1', ..., a_k - a_k'\) have to be zero, as \(v_1, ..., v_k\) are linearly independent, therefore \(a_1 = a_1', ..., a_k = a_k'\), hence their corresponding linear combinations are unique.
\end{proof}

\begin{theorem}
Suppose there exists a finite set \(S\) such that \(span(S) = V\). Then \(V\) has a basis given by a subset of S.
\end{theorem}

\begin{proof}
If \(S = \varnothing\) or \(S = \{0\}\), then \(V = span(S) = \{0\}\) and \(\beta = \varnothing\) is a basis. If this is not the case, then \(S\) has at least one non-zero vector \(v_1\). \(\{v_1\}\) is linearly independent. Proceed by induction:

Suppose \(v_1, ..., v_k \in S\) are distinct and linearly independent. 
\begin{itemize}
\item If \(S \nsubseteq span\{v_1,...,v_n\}\), there exists \(v_{k + 1} \in S, v_{k + 1} \notin span\{v_1,...,v_k\}\). By basic property (4),\(\{v_1,...,v_{k+1}\}\) is linearly independent. Proceed with induction.
\item If \(S \subseteq span\{v_1,...,v_n\}\), then \(span(S) = V \subseteq span\{v_1,...,v_k\}\). Stop the induction.
\end{itemize}
Eventually, the induction has to stop, one way or another, because \(S\) is finite. At its conclusion, we obtain a subset of \(S\) which is a basis of \(V\) (spanning \(V\) and linearly independent).
\end{proof}

\begin{fact}
Every vector space \(V\) has a basis \(\beta\) (involves axiom of choice). In this course we will take this fact for granted.
\end{fact}

\begin{theorem}
If \(\beta, \gamma\) are two bases of \(V\), then \(\#\beta = \#\gamma\). We allow for the possibility of an infinite basis (e.g. all bases will be infinite)
\end{theorem}

\begin{definition}
Given a basis \(\beta\) for \(V\), one calls \(\#\beta = dim(V)\) the \underline{dimension of \(V\)}
\end{definition}

Examples:
\begin{enumerate}
\item \(dim(F^n) = n\)
\item \(dim(P_n(\mathbb{R})) = n + 1\)
\item \(dim(M_{n \times m}(F)) = nm\)
\item \(dim(F^{\infty}) = \infty\), \(dim(F^{\infty}_{fin}) = \infty\)
\item \(dim(\mathcal{F}(\mathbb{R},\mathbb{R})) = \infty\).
\end{enumerate}

\begin{lemma}
Replacement lemma: Suppose \(S\) is a finite subset of \(V\) which spans \(V\). Let \(v_1, ..., v_m \in V\) be linearly independent. Then there are distinct vectors \(u_1,...,u_m \in S\) such that \((S \setminus \{u_1,...,u_m\}) \cup \{v_1,...,v_m\}\) again spans \(V\).
\vspace{2mm}

\underline{In particular, this means \(\#S \geq m\)}.
\end{lemma}

\begin{proof}
(Theorem 3): If \(\#\beta = \infty = \#\gamma\) there is nothing to show. So, we assume \(\beta\) or \(\gamma\) is finite. Let's say \(\#\beta < \infty\). Applying the replacement lemma to \(S = \beta\), and distinct \(v_1,...,v_m \in \gamma\), shows \(\#\beta \geq m\). Since \(v_1,...,v_m\) are arbitrary, we get \(\#\beta \geq \#\gamma\), implying in particular \(\#\gamma < \infty\). Reversing the roles of \(\beta\) and \(\gamma\), we obtain \(\#\gamma \geq \#\beta \implies \#\beta = \#\gamma\).
\end{proof}

\begin{proof}
(Replacement Lemma:) By induction.

If \(m = 0\), there is nothing to show (\(S\) does not change)

Assume the statement of the lemma holds for a given \(m\), we prove it for \(m + 1\):

\(v_1,...,v_{m+1}\) are linearly independent. By induction hypothesis applied to \(v_1,...,v_m \exists\) distinct vectors \(u_1,...,u_m \in S\) such that \((S \setminus \{u_1,...,u_m\}) \cup \{v_1,...,v_m\}\) spans \(V\). In particular, \(v_{m + 1} \in V = span(S)\) means we can write \(v_{m + 1} = a_1v_1 + ... + a_mv_m + b_1w_1 + ... + b_rw_r\) where \(w_1,...,w_r \in S\). All \(b_1,...,b_r\) cannot be zero, as otherwise \(v_{m+1}\) would be a linear combination of \(v_1,...,v_m\) which is impossible as these are linearly independent. Hence, one of \(b_1,...,b_r\) must be nonzero. Let us call this coefficient \(b_i \neq 0\). Let \(u_{m+1} = w_i\) for this particular \(i\). \(u_{m+1} = w_i = -\frac{1}{b_i}(a_1v_1 + ... + a_mv_m + b_1w_1 + ... b_{i-1}w_{i-1} + b_{i+1}w_{i+1} + ... + b_rw_r + v_{m + 1}\). The point of writing it like this is that we now see that \(u_{m+1} \in span((S \setminus \{u_1,...,u_m,u_{m+1}\}) \cup \{v_1,...,v_m,v_{m+1})\). Let this new set be called \(S'\).

Finally, \(span(S') = span((S \setminus \{u_1,...,u_{m+1}\}) \cup \{v_1,...,v_{m+1}) = span((S \setminus \{u_1,...,u_{m+1}\}) \cup \{v_1,...,v_m\} \supseteq span((S \setminus \{u_1,...,u_m\}) \cup \{v_1,...,v_m\} = V\).
\end{proof}

\end{document}