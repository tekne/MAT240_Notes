\documentclass{article}
\title{MAT240 Notes: Matrix Multiplication}
\author{Jad Elkhaleq Ghalayini}
\date{October 31 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\begin{document}
\maketitle

Matrix multiplication represents the composition of linear maps. Matrix multiplication has the following structure:
\[M_{m \times n}(F) \times M_{n \times l}(F) \to M_{m \times l}(F), (A,B) \mapsto C = AB\]
\[\begin{pmatrix}A_{11} & \dots & A_{1n} \\ \vdots & \ddots & \vdots \\ A_{m1} & \dots & A_{mn}\end{pmatrix}\begin{pmatrix}B_{11} & \dots & B_{1n} \\ \vdots & \ddots & \vdots \\ B_{m1} & \dots & B_{mn}\end{pmatrix} = \begin{pmatrix}C_{11} & \dots & C_{1n} \\ \vdots & \ddots & \vdots \\ C_{m1} & \dots & C_{mn}\end{pmatrix}\]
To get the \(C_{ik}\), we multiply the \(i^{th}\) row of \(A\) by the \(k^{th}\) column of \(B\), that is 
\[C_{ik} = \sum^n_{j=1}A_{ij}B_{jk}\]

Remarks:
\begin{enumerate}
    \item Scalar multiplication of matrices \(A \mapsto \lambda A\) means, by definition, we multiply all elements in \(A\) by \(\lambda\). We can regard this as a matrix multiplication by \[\begin{pmatrix}\lambda & 0 & \dots & 0\\ 0 & \lambda & \dots & 0\\\vdots & \vdots & \ddots & \vdots\\0 & 0 & \dots & \lambda\end{pmatrix}\]
    
    \item Applying a matrix to a vector can be regarded as a spacial case of matrix multiplication: multiplying the vector (as a column vector) by the matrix like-so: \(Ax = y\) (e.g. vector on the \underline{right}, matrix on the \underline{left})
    
    \item  So any \(A \in M_{m \times n}(F)\) defines linear map \(L_A: F^n \to F^m, x \mapsto Ax = y\). This identifies the isomorphism \[M_{m \times n}(F) \cong \mathcal{L}(F^n,F^m), A \mapsto L_A\]
    Note: The \(j^{th}\) column of \(A\) is obtained as \(Ae_{j}\) where \(e_{j}\) is the \(j^{th}\) standard basis vector of \(F^n\) (everywhere 0 except for a 1 at the \(j^{th}\) entry) 
    
    \item By construction, \(L_{AB} = L_A \circ L_B\)
    
    \item Matrix multiplication is \underline{bilinear}: \[(A_1 + A_2)B = A_1B + A_2B, (aA)B = aAB\]\[ A(B_1 + B_2) = AB_1 + AB_2, A(bB) = bAB\]
    
    \item Matrix multiplication is not commutative, \(AB \neq BA\) (like composition), even if it makes sense to compare them (and both are defined!). Also, \(AB = 0\) does NOT imply \(A = 0\) or \(B = 0\). For example, take \(A = B = \begin{pmatrix} 0 & 1 \\ 0 & 0\end{pmatrix} \neq 0\). But \(AB = 0\).
    
    \item Matrix multiplication is associative: \((AB)C = A(BC)\) (again like composition)
    
    \item Reminder: If \(T: V \to W\) is a linear map, \(\beta = \{v_1,...,v_n\}\) is a basis of \(V\) and \(\gamma = \{w_1,...,w_m\}\) is a basis of W, then \([T]^\gamma_\beta\) is the matrix whose \(j^{th}\) columns are coefficients of \(T(v_j)\) with respect to \(w_1,...,w_m\).
    \[[T(v_j)]_\gamma = [T]^\gamma_\beta[v]_\beta\]
    \[[T \circ S]^\gamma_\alpha = [T]^\gamma_\beta[S]^\beta_\alpha, S: U \to V\]
    
    Special case: \(V = W, \beta = \gamma\). New notation: \([T]^\gamma_\beta = [T]_\beta\)
\end{enumerate}

How do matrices (and vectors!) change when the basis (coordinates!) do?

Let \(V\) be a vector space and suppose we have two bases, \(\beta, \beta'\). Given \(v \in V\), what is the relation between \([v]_\beta\) and \([v]_{\beta'}\)?

Consider the special case of \(T: V \to W\): the \underline{identity} map \(I_V: V \to V\).
\[[v]_{\beta'} = [I_V]^{\beta'}_\beta[v]_\beta\]

We call \(Q = [I_V]^{\beta'}_\beta\) the "change of coordinate matrix" (from \(\beta\) to \(\beta'\)).

The change of co-ordinate matrix from \(\beta'\) to \(\beta\) is \([I_V]^\beta_{\beta'}\). Claim: This is \(Q^{-1}\). Particularly, this tells us \(Q\) is invertible. To check this, we just multiply the matrices:
\[[I_V]^{\beta'}_\beta[I_V]^\beta_{\beta'} = [I_V]^{\beta'}_{\beta'} = \begin{pmatrix}1 & 0 & \dots & 0 \\ 0 & 1 & \dots & 0 \\ \vdots & \vdots & \ddots & \vdots \\ 0 & 0 & \dots & 1\end{pmatrix}\]

The columns of \(Q\) are the coefficients of \(v_j\) (\(T(v_j)\) with \(T = I_V\)) in terms of the new basis \(\beta' = \{v_1', v_2',...\}\).

Example: Let \(V = \mathbb{R}^2\), with basis \[\beta = \{v_1 = (\cos(\theta),\sin(\theta)), v_2 = (-\sin(\theta),\cos(\theta))\}\] for a given \(\theta\). Choosing instead \(\theta'\), we get a new basis \(\beta'\) What's the change of basis matrix?

We can see geometrically the bases \(\beta, \beta'\) are formed by rotating the standard basis by \(\theta, \theta'\) respectively. So we expect they are related by a rotation by angle \(\theta - \theta'\) (or \(\theta' - \theta\)) Try: \[\cos(\alpha)v_1 + \sin(\alpha)v_2 = \cos(\alpha)(\cos(\theta),\sin(\theta)) + \sin(\alpha)(-\sin(\theta),\cos(\theta))\]\[ = (\cos(\alpha + \theta),\sin(\alpha + \theta)\] (using trig identities). Again, we try \[-\sin(\alpha)v_1 + \cos(\alpha)v_2 = (-\sin(\alpha + \theta), \cos(\alpha + \theta))\] (calculations omitted) Here we guessed basically guessed what the answer was, the above being correct for \(\alpha = \theta' - \theta\), giving basis \(v_1',v_2'\).

So \[v_1' = \cos(\theta' - \theta)v_1 + \sin(\theta' - \theta)v_2, v_2' = -\sin(\theta' - \theta)v_1 + \cos(\theta' - \theta)v_2\]

So \[[I_V]^\beta_{\beta'} = \begin{pmatrix}\cos(\theta' - \theta) & -\sin(\theta' - \theta) \\ \sin(\theta' - \theta) & \cos(\theta' - \theta)\end{pmatrix}, [I_V]^{\beta'}{\beta} = \begin{pmatrix}\cos(\theta - \theta') & -\sin(\theta - \theta') \\ \sin(\theta - \theta') & \cos(\theta - \theta')\end{pmatrix}\] We can see these matrices are inverses of each other. Each of these represents a rotation.

Example: Lagrange Interpolation, \(F = \mathbb{R}, V = Pol_n(\mathbb{R})\). Let \(c_0, c_n \in \mathbb{R}\) be distinct. Recall that these determine Lagrange interpolation polynomials denoted \(p_0, ..., p_n \in Pol_n(\mathbb{R})\) such that each is the unique polynomial where \(p_i(c_j) = 0, j \neq i, p_i(c_i) = 1\). These are a basis of \(Pol_n(\mathbb{R})\).

What's the change of basis matrix from this basis to the standard basis or vice versa?

Given \(q \in Pol_n(\mathbb{R})\), we have \(q(x) = q(c_0)p_0(x) + q(c_1)p_1(x) + ... + q(c_n)p_n(x)\) As a special case, apply this to \(q(x) = \{1,x,x^2,...,x^n\}\).
\[1 = p_0(x) + ... + p_n(x), x = c_0p_0(x) + ... + c_np_n(x), x^2 = c_0^2p_0(x) + ... + c_n^2p_n(x), ...\]
We hence obtain
\[Q = [I_V]^{\beta'}_\beta = \begin{pmatrix}1 & c_0 & c_0^2 & \dots & c_0^n \\ 1 & c_1 & c_1^2 & \dots & c_1^n \\ \vdots & \vdots & \vdots & \ddots & \vdots \\ 1 & c_n & c_n^2 & \dots & c_n^n\end{pmatrix}\]

Note in particular that a matrix of this form (with \(c_0, ..., c_n\) all distinct) must be \underline{invertible}. \(Q^{-1}\) is change of basis matrix for \(\beta'\) to \(\beta\). Its \(j^th\) column is the coefficients of \(p_j\) in term of \(1, x, ..., x^n\).

\vspace{5mm}

Given \(T \in \mathcal{L}(V)\), how does \([T]_\beta\) change when we replace \(\beta\) with \(\beta'\)?

\begin{theorem}
Let \(\beta, \beta'\) be two bases of \(V\). Then the change of co-ordinate matrix \(Q = [I_V]^{\beta'}_{\beta}\) is invertible, with inverse \(Q^{-1} = [I_V]^\beta_{\beta'}\). Given \(v \in V\), we have \([v]_{\beta'} = Q[v]_\beta\).

Given \(T \in \mathcal{L}(V)\), have \([T]_{\beta'} = Q[T]_{\beta}Q^{-1}\)
\end{theorem}

\begin{proof}
Proof of the last identity: \[[T]_{\beta'} = [T]^{\beta'}_{\beta'} = [I_V]^{\beta'}_{\beta}[T]^\beta_\beta[I_V]^\beta_{\beta'} = Q[T]_\beta Q^{-1}\] 
\end{proof}

\begin{definition}
Matrices \(A', A \in M_{m \times n}(F)\) are \underline{similar} if there exists \(Q \in M_{m \times n}(F)\) such that \(A' = QAQ^{-1}\)
\end{definition}
\end{document}