My notes from MAT240 (LEC0101, Professor Eckhard Meinrenken) in LaTeX. Warning: May be incomplete/incorrect!

Licensed under the BSD 3-clause license. Please feel free to contribute! Compiled using ```latexmk --pdf```.

Course website: http://www.math.toronto.edu/mein/teaching/MAT240/MAT240.html