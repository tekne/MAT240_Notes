\documentclass{article}
\title{MAT240 Notes: Linear Transformations}
\author{Jad Elkhaleq Ghalayini}
\date{October 19 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\begin{document}
\maketitle

\begin{definition}
Let \(V, W\) be vector spaces over a field \(F\). A function \(T: V \to W\) is called a \underline{linear transformation} (\underline{linear map}) if \[T(v_1 + v_2) = T(v_1) + T(v_2) \forall v_1, v_2 \in V, T(av) = aT(v) \forall v \in V, a \in F\]
\end{definition}

\begin{note}
For a linear transformation \(T: V \to W\),
\begin{itemize}
    \item \(T(0) = 0_W\) (follows from \(T(av) = aT(v)\) by taking \(a = 0\))
    \item \(T: V \to W\) is linear \(\iff T(a_1v_1 + ... + a_kv_k) = a_1T(v_1) + ... + a_kT(v_k)\)
\end{itemize}
\end{note}

Examples:
\begin{enumerate}
    \item If \(W = {0}\) then \underline{any} \(T: V \to W\) is going to be a linear transformation
    \item \(T: \mathbb{R}^3 \to \mathbb{R}^3, (t_1,t_2,t_3) \mapsto (t_1,t_2,0)\)
    \item \(T: \mathbb{R}^2 \to \mathbb{R}^3, (t_1,t_2) \mapsto (t_1,t_2,0)\). You can think of this as the inclusion of the \(xy\) plane into \(xyz\) space
    \item \(T: \mathbb{R} \to \mathbb{R}^3, t \mapsto (t,t,t)\)
    \item \(T: \mathbb{R}^3 \to \mathbb{R}, (t_1,t_2,t_3) \to 4t_1 - 3t_2 + 7t_3\)
    \item \(T: V \to V, v \mapsto -v\)
    \item \(T: M_{m \times n}(F) \to M_{n \times m}(F), A \mapsto A^T\)
    \item \(T: \mathcal{F}(\mathbb{R},\mathbb{R}) \to \mathbb{R}, f \mapsto f(1)\)
    \item \(T: Pol_n(\mathbb{R}) \to Pol_{n-1}(\mathbb{R}), p \mapsto p'\)
    \item \(T: Pol_n(\mathbb{R}) \to Pol_{n+1}(\mathbb{R}), p \mapsto \int_0^xp(t)dt\) (Indefinite integrals require a quotient space)
    \item \(T: Pol_n(\mathbb{R}) \to Pol(\mathbb{R}), p \mapsto p''' + 2p'' - p\)
    \item \(T: F^\infty \to F^\infty, (a_1,a_2,a_3,...) \mapsto (0,a_1,a_2,a_3,...)\)
    \item Any vector space \(V\) has two very important linear transformations: \(T: V \to W, v \mapsto 0\) "zero transform" and \(T: V \to V, v \mapsto v\), the "identity transform".
\end{enumerate}

Non Examples:
\begin{enumerate}
    \item \(T: \mathbb{R} \to \mathbb{R}^3, t \mapsto (t,t^2,t^3)\) is an example of a \underline{nonlinear map}. \(T: \mathbb{R} \to \mathbb{R}, t \mapsto t^2\) is decidedly nonlinear (it is quadratic)
    \item The "line" \(\mathbb{R} \to \mathbb{R}, x \mapsto mx + b\) is \underline{not} linear, except for b = 0 (it is an \underline{affine linear map} though). The quickest way to check this is that 0 does not necessarily go to 0 (it goes to \(b\))
\end{enumerate}

\begin{definition}
For a linear map \(T: V \to W\) are defined
\begin{itemize}
    \item The null space, \(N(T) = \{v \in V: T(v) = 0\}\). \(N(T) = T^{-1}(\{0\})\) (the pre-image of 0 under the map \(T\))
    \item The range (or image) \(\{R(T) = \{w \in W: \exists v \in V: T(v) = w\}\) (\(R(T) = T(V) \subseteq W\))
\end{itemize}
\end{definition}

\begin{note}
\(N(T)\) is a subspace of \(V\) and \(R(T)\) is a subspace of \(W\) (since both are closed under addition and scalar multiplication)
\end{note}

E.g. for \(w_1,w_2 \in R(T)\) then \(w_1 = T(v_1), w_2 = T(v_2)\), so \(w_1 + w_2 = T(v_1 + v_2)\) and so we see the result is again in the range. Likewise, we can check if the range is closed under scalar multiplication, and likewise for the nullspace.

Examples:
\begin{enumerate}
    \item \(T: M_{3 \times 3}(\mathbb{R}) \to M_{3 \times 3}(\mathbb{R}), A \to A^T + A\). \(N(T) = \{\)skew-symmetric matrices\(\}\), \(R(T) = \{\)symmetric matrices\(\}\) (Reason \(A^T + A)^T = A + A^T\) so we see that the transpose of anything in the image of the transform is equal to the original object. Then \(R(T) \subseteq\) symmetric matrices. Conversely, if \(B = B^T\), then \(A = \frac{1}{2}B\) satisfies \(T(A) = \frac{1}{2}B^T + \frac{1}{2}B = B\) (since \(B^T = B\)). If we replace \(\mathbb{R}\) by another field, everything goes through unless \(1 + 1 = 0\) (as we divide by 2). 
    \item \(T: Pol_{11}(\mathbb{R}) \to Pol(\mathbb{R}, p \mapsto p'''\). \(N(T) = Pol_2(\mathbb{R}), R(T) = Pol_8(\mathbb{R})\)
\end{enumerate}

\begin{theorem}{Rank Nullity Theorem (aka. Dimension Theorem): } \(\forall\) linear \(T: V \to W\), \(dim(R(T)) + dim(N(T)) = dim(V)\) 
\end{theorem}

Example: \(V = Pol_11(\mathbb{R}, T(p) = P''', dim(V) = 12, dim(R(T)) = 9, dim(N(T)) = 3, 9 + 3 = 12\)

\end{document}