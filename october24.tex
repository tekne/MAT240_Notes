\documentclass{article}
\title{MAT240 Notes: Linear Transformations}
\author{Jad Elkhaleq Ghalayini}
\date{October 24 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\begin{document}
\maketitle

\begin{itemize}
    \item \(T\) is surjective (onto) \(\iff R(T) = W\)
    \item \(T\) is injective (one-to-one) \(\iff N(T) = \{0\}\)*
    \item \(T\) is bijective (invertible) \(\iff\) \(T\) is injective and surjective. Bijective linear maps are also called \underline{linear isomorphisms}
\end{itemize}

*Explanation: \(T\) being injective as a map between sets means by definition that \(T(v_1) = T(v_2) \iff v_1 = v_2 \implies T(v) = 0 = T(0) \iff v = 0 \iff N(T) = \{0\}\) For the other direction of the if and only if, if \(N(T) = \{0\}\), then \(T(v_1) = T(v_2) \implies T(v_1 - v_2) = 0 \iff v_1 - v_2 = 0 \iff v_1 = v_2\).

\begin{theorem}
If \(T: V \to W\) is a linear isomorphism, \(T^{-1}: W \to V\) is again a linear map
\end{theorem}

\begin{proof}
\[T(T^{-1}(w_1) + T^{-1}(w_2)) = T(T^{-1}(w_1)) + T(T^{-1}(w_2)) = w_1 + w_2 = T(T^{-1}(w_1 + w_2))\] since \(T\) is linear

Since \(T\) is injective, because these values are the same, the arguments must be the same. Hence, \(T^{-1}(w_1) + T^{-1}(w_2) = T^{-1}(w_1 + w_2)\). Similar for scalar multiplication.
\end{proof}

\begin{theorem}
\underline{Rank-nullity theorem (dimension theorem):} Let \(T: V \to W\) be a linear map, \(dim(V) < \infty\). Then \(nullity(T) + rank(T) = dim(V)\)
\end{theorem}

Remark: Also true if \(dim(V) = \infty\)

\begin{proof}
Let \(v_1,...,v_k\) be a basis of \(N(T)\). Let \(v_1,...,v_k,v_{k+1},...,v_n\) be an extension to a basis of \(V\). Claim: \(T\) of the last \(n - k\) vectors is going to be a basis of the range. (This will prove the theorem, because then \(dim(R(T)) = n - k =  dim(V) - dim(N(T))\))

Proof of the claim
\begin{itemize}
    \item \(T(v_k),...,T(v_n)\) spans \(R(T)\): 
    
    Let \(w \in R(T)\), so \(w = T(v)\). Write \(v = a_1v_1 + ... + a_nv_n\) then \(w = T(v) = (a_1T(v_1) + ... + a_k(T(v_k)) = 0) + a_{k+1}T(v_{k+1}) + ... + a_nT(v_n)\)
    \item \(T(v_k),...,T(v_n)\) is linearly independent: 
    
    Suppose \(a_{k+1}T(v_{k+1}) + ... + a_nT(v_n) = 0\). Since \(T\) is linear, we can write \(T(a_{k+1}v_{k+1} + ... + a_nv_n) \implies a_{k+1}v_{k+1} + ... + a_nv_n\). And we have a basis for the null space, \(v_1,...,v_k\). i.e. \(a_{k+1}v_{k+1} + ... + a_nv_n = b_1v_1 + ... + b_kv_k \iff -b_1v_1 - ... - b_kv_k + a_{k+1}v_{k+1} + ... + a_nv_n = 0\) which, by linear independence, means all \(b_1,...,b_k\) and \(a_{k+1},...,a_n\) must be 0.
\end{itemize}
\end{proof}

\subsection*{Consequences}
\begin{theorem}
If \(T: V \to W\) is a linear isomorphism, then \(dim(V) = dim(W)\).
\end{theorem}

Note: an isomorphism is a map which is invertible and preserves a structure (e.g. linearity for a vector space)

\begin{proof}
If \(T\) is an isomorphism, then \(N(T) = 0, R(T) = W\), so \(nullity(T) = 0, rank(T) = dim(W)\), so \(dim(V) = 0 + dim(W) = dim(W)\) by the Rank Nullity Theorem.
\end{proof}

\begin{theorem}
Suppose \(T: V \to W\) is linear, \(dim(V) = dim(W) < \infty\) (only true for finite dimensional case this time). Then \(T\) is injective \(\iff T\) is surjective \(\iff T\) is bijective
\end{theorem}

\begin{proof}
\(T\) injective \(\iff nullity(T) = 0 \iff rank(T) = dim(V) = dim(W) \iff R(T) = W\) (meaning \(T\) is surjective).
\end{proof}

Concrete counterexample: For \(dim(V) = \infty\), this can be false. Consider the map \(T: F^\infty \to F^\infty, (a_1,a_2,a_3,...) \mapsto (0,a_1,a_2,...)\) - "right shift". \(T\) is injective (if something maps to 0, \(a_1 = a_2 = ... = 0\)) but not surjective (not everything is in the image, e.g. everything where the first component is not 0).

\(S: F^\infty \to F^\infty, (a_1,a_2,a_3,...) \mapsto (a_2,a_3,a_4,...)\) - "left shift". \(S\) is surjective but not injective, as every sequence can be obtained in this way, because all nonzero elements \((a,0,0,...)\) map to 0)

\begin{definition}
\(\mathcal{L}(V,W)\) is the set of linear maps from \(V\) to \(W\), and is itself a vector space over \(F\).

Given \(S,T \in \mathcal{L}(V,W), a \in F\), we define \[(S + T)(v) = S(v) + T(v), (aT)(v) = aT(v)\] Technically we now have to check the vector space axioms, but this is left as an optional, easy exercise.
\end{definition}

Special cases:
\begin{itemize}
    \item Recall that \(F\) is a vector space over itself. \(\mathcal{L}(F,W) \cong W\) given by \(T \mapsto T(1)\)
    \begin{itemize}
        \item We have to check this is an element of \(\mathcal{L}(\mathcal{L}(F,W),W)\) (that it is linear). 
        \item We have to check the map is injective, i.e. \(T(1) = 0 \implies T = 0\)
        \item We have to check the map is surjective: given \(w \in W\), there exists\(T \in \mathcal{L}(F,W)\) with \(T(1) = w\).Indeed, take \(T(a) = aw\).
    \end{itemize}
    \item \(\mathcal{L}(V,F)\) is called the \underline{dual space} to \(V\), \(V*\). (Can also define vector space \(\mathcal{L}(V,\mathcal{L}(W,U))\)
\end{itemize}

\section{Matrix Representations of Linear Maps}
We assume \(dim < \infty\).

Given \(V\) with basis \(\beta = \{v_1,...,v_n\}\) , every \(v\) can be uniquely expressed as \(v = a_1v_1 + ... a_nv_n\). All the information in \(v\) is stored in \(a_1,...,a_n\).

So we write \[[v]_\beta = \begin{pmatrix}a_1 \\ \vdots \\ a_n\end{pmatrix}\] the "coordinate vector of \(v\) in a given basis".
The map \(V \to F^n, v \mapsto [v]_\beta\) is an isomorphism of vector spaces, with inverse \[\begin{pmatrix}a_1 \\ \vdots \\ a_n\end{pmatrix} \mapsto a_1v_1 + ... + a_nv_n\]

Given \(V\) with basis \(v_1,...,v_n\), \(W\) with basis \(w_1,...,w_m\), we can express any \(T \in \mathcal{L}(V,W)\) in terms of bases:
\[T(v_1) = A_{11}w_1 + A_{21}w_2 + ... + A_{m1}w_m, T_(v_2) = A_{12}w_1 + A_{22}w_2 + ... + A_{m2}w_m\]
\[T(v_n) = ...\]
In short, \[T(v_j) = \sum_{i = 1}^mA_{ij}w_i\]
We get this panel of numbers \(A_{11},...,A_{mn}\). We quickly get too lazy to write down these sums, so we write down the matrix. This defines the \underline{matrix of T} with respect to the bases \(\beta, \gamma\):
\[[T]^\gamma_\beta = \begin{pmatrix}
A_{11} & \cdots & A_{1n} \\ \vdots & \ddots & \vdots \\ A_{m1} & \cdots & A_{mn}
\end{pmatrix} \in M_{m \times n}(F)\]

\(n = dim(V), m = dim(W)\).

Note: the coefficients of \(w_1,...,w_m\) are the \underline{columns} of the matrix

Example: \(V = Pol_3(\mathbb{R}), W = Pol_4(\mathbb{R})\), \[T: V \to W, p(x) \mapsto p'(x) + \int_0^xp(t)dt\] (Warning: this notation is somewhat sloppy, we are mapping the \underline{polynomial}, not the values)

Write matrix of \(T\) with respect to the standard basis.

We calculate: \(T(1) = x, T(x) = 1 + \frac{x^2}{2}, T(x^2) = 2x + \frac{x^3}{3}, T(x^3) = 3x^2 + \frac{x^4}{4}\)

Writing as a matrix, we obtain
\[\begin{pmatrix}
0 & 1 & 0 & 0 \\
1 & 0 & 2 & 0 \\
0 & \frac{1}{2} & 0 & 3 \\
0 & 0 & \frac{1}{3} & 0 \\
0 & 0 & 0 & \frac{1}{4}
\end{pmatrix}\]

What's the relationship between \([T]^\gamma_\beta, [v]_\beta, [w]_\gamma\)?

\[[v]_\beta = \begin{pmatrix}a_1 \\ \vdots \\ a_n\end{pmatrix}, [w]_y = \begin{pmatrix}b_1 \\ \vdots \\ b_m\end{pmatrix}\]

We calculuate:
\[T(v) = T(\sum_{j = 1}^na_jv_j) = \sum_{j = 1}^na_jT(v_j) = \sum_{j = 1}^na_j\sum_{i = 1}^mA_{ij}w_i = \sum_{i = 1}^m(\sum_{j = 1}^mA_{ij}a_j)w_i\]
\[b_i = \sum_{j = 1}^nA_{ij}a_j\]

One can write this as \[
\begin{pmatrix}
A_{11} & \cdots & A_{1n} \\ \vdots & \ddots & \vdots \\ A_{m1} & \cdots & A_{mn}
\end{pmatrix}
\begin{pmatrix}
a_1 \\ \vdots \\ a_n
\end{pmatrix}
= \begin{pmatrix}
b_1 \\ \vdots \\ b_m
\end{pmatrix}
\]

The upshot is: Once basis of \(V,W\) are chosen, this choice identifies \(V \cong F^n\), \(W \cong F^m\), \(\mathcal{L}(V,W) \cong M_{m \times n}(F)\).

\begin{theorem}
The map \(\mathcal{L}(V,W) \to M_{m \times n}(F), T \mapsto [T]_\beta^\gamma\) is an isomorphism of vector spaces.
\end{theorem}

\begin{proof}
The map is linear,because \([T_1 + T_2]_\beta^\gamma = [T_1]_\beta^\gamma + [T_2]_\beta^\gamma\)
Likewise, \([aT]_\beta^\gamma = a[T]_\beta^\gamma\).

It's injective (one to one) because if \([T]_\beta^\gamma = 0\) means\(T(v_j) = 0\) for all \(j\), hence \(T = 0\).

It's surjective (onto) because any \(A \in M_{m \times n}(F)\) defines a \(T \in \mathcal{L}(V,W)\) with \([T]_\beta^\gamma = A\) by taking \(T(v_j) = \sum_{i = 1}^m A_{ij}W_i\)
\end{proof}

In particular, we see: \(dim(\mathcal{L}(V,W)) = dim(V) \cdot dim(W)\) because \(dim(M_{m \times n}(F)) = n \cdot m\)

Special cases: \(\dim(\mathcal{L}(F,W)) = 1 \cdot dim(W) = dim(W)\) (as it is isomorphic to \(W\)), \(\dim(V*) = dim(\mathcal{L}(V,F)) = dim(V) \cdot 1 = dim(V)\).
\end{document}