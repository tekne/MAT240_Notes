\documentclass{article}
\title{MAT240 Notes: Determinants}
\author{Jad Elkhaleq Ghalayini}
\date{November 28 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}{Proposition}
\newtheorem{lemma}{Lemma}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\begin{document}
\maketitle

Recall that
\[A^{-1} = \begin{pmatrix} a & b \\ c & d \end{pmatrix}^{-1} = \frac{1}{ad - bc}\begin{pmatrix} d & -b \\ -c & a \end{pmatrix} = \frac{1}{\det(A)}\begin{pmatrix} d & -b \\ -c & a \end{pmatrix}\]

Let \(vol(v_1, v_2)\) denote the \underline{signed} volume of the parallelogram formed by two vectors: positive if the angle between the vectors is between \(0\) and \(\pi\), negative if the angle is between \(\pi\) and \(2\pi\).

\begin{proposition}
The function \(vol: \mathbb{R}^2 \times \mathbb{R}^2 \to \mathbb{R}\) is bilinear (linear in each argument) and satisfies \(vol(v, v) = 0 \forall v \in V\).
\end{proposition}

Let's calculate \(vol(v_1, v_2)\) for \(v_1 = \begin{pmatrix}a & c\end{pmatrix}^T, v_2 = \begin{pmatrix}b & d\end{pmatrix}^T\).
\[vol(v_1, v_2) = vol(ae_1 + ce_2, be_1 + de_2) = avol(e_1, be_1 + de_2) + cvol(e_1, be_1 + de_1)\]\[ = abvol(e_1, e_1) + advol(e_1, e_2) + bcvol(e_2, e_1) + cdvol(e_1,e_2)\]
We define \(vol(e_1, e_2) = 1, vol(e_2, e_1) = -1\), and hence obtain
\[vol(v_1, v_2) = ad - bc\]

\begin{note}
\(vol(v_1, v_1) = 0 \implies vol(v_1, v_2) = -vol(v_2, v_1)\). To see this, expand
\[0 = vol(v_1 + v_2, v_1 + v_2)\]\[ = vol(v_1, v_1) + vol(v_1, v_2) +  vol(v_2, v_1) + vol(v_2, v_2)\]\[ = vol(v_1, v_2) +  vol(v_2, v_1)\]
\end{note}
This makes sense for any field \(F\), but the geometric interpretation as a volume gets lost along the way. We call the map \(\det: F^2 \times F^2 \to F\), with \(\det(v, v) = 0, \det(e_1, e_2) = 0\).
\section{Generalization to higher dimensions}
In higher dimensions, we would look at \(vol(v_1,...,v_n)\): the volume of a parallelepiped spanned by \(v_1,...,v_n \in \mathbb{R}^n\). This has properties analogous to those for \(n = 2\), e.g. \underline{multilinearity} (versus bilinearity), e.g. linear at each argument if the other arguments are kept fixed, and is zero if any two arguments coincide. 

\begin{theorem}
If \(F\) is any field there is a unique multilinear map 
\[\det: F^n \times ... \times F^n \to F\]
with the properties that \(\det(v_1,...,v_n) = 0\) if \(v_i = v_j\) for any \(i \neq j\) and \(\det(e_1,...,e_n) = 1\).
\end{theorem}

\subsection{Permutations}
A permutation of \(\{1,...,n\}\) is simply an invertible map. Imagine a deck of cards numbered from \(1,...,n\), shuffled. Each card goes to a new position, but you can always send it back to the original ordering. So we have an invertible map
\[\sigma: \{1,...,n\} \to \{1,...,n\}, i \mapsto \sigma(i)\]
Example: \(\{1, 2, 3, 4\}\),
\[\sigma(1) = 4, \sigma(2) = 3, \sigma(3) = 1, \sigma(4)= 2\] often depicted as \((4, 3, 1, 2)\).

A permutation is \underline{even} or \underline{odd} depending on whether the set of pairs \[\#\{(\sigma(i),\sigma(j))| i < j, \sigma(i) > \sigma(j)\}\] is even or odd. If it's even, write \(sign(\sigma) = 1\), if it's odd, write \(sign(\sigma) = -1\).
In the example, the pairs in the wrong order are \((4, 3), (4, 1), (4, 2), (3, 1), (3, 2)\), giving five pairs. Hence, \(\sigma\) is odd. So \(sign(\sigma) = -1\).
Modified example: \((4, 1, 3, 2)\). Now we have pairs \((4, 1), (4, 3), (4, 2), (3, 2)\) giving four pairs. Hence, \(\sigma\) is even, so \(sign(\sigma) = 1\). 
Note that if we switch two adjacent elements, all the orders remain the same except for those two adjacent elements, and hence the \(sign\) of a permutation changes sign.

Exercises: 
\begin{itemize}
    \item Show that even if the elements are not adjacent, if \(\sigma'\) is obtained from \(\sigma\) by interchanging two elements, then \(sign(\sigma') = -sign(\sigma)\).
    \item For any permutation \(\sigma\) have \(sign(\sigma) = (-1)^N\) where \(N\) is the number of interchanges needed to put \(\sigma\) back to \(\{1,...,n\}\)
\end{itemize}

Theorem 1:
\begin{proof}
We'll first prove uniqueness, assuming existence. As in case \(n = 2\), the property that \(\det(...) = 0\) whenever \(v_i = v_j\) for some \(i \neq j\) implies that \(\det\) changes sign if we interchange two elements, e.g. \(\det(v_1,v_2,...,v_n) = -\det(v_2,v_1,...,v_n)\) (using \(0 = \det(v_1 + v_2, v_1, + v_2, v_3,...) = ...\) as in the \(n = 2\) case).

Write \(v_1,...,v_n\) in terms of the standard basis:
\[v_1 = \sum^n_{i_1 = 0}A_{i_11}e_{i_1}, v_2 = \sum^n{i_2 = 0}A_{i_22}e_{i_2}, ...\]
Then \[\det(v_1,...,v_n) = \sum_{i_1 = 0}^n...\sum_{i_n}^n\det(A_{i_11}e_{i_1},A_{i_22}e_{i_2},...) = \sum_{i_1,...,i_n}A_{i_11}...A_{in_n}\det(e_{i_1},...,e_{i_n})\]
\(\det(e_{i_1},...,e_{i_n})\) is zero unless \(i_1,...,i_n\) are a permutation of \(1,...,n\) as otherwise there must be repetitions. Let us rewrite it in this manner:
\[\sum_{\sigma}A_{\sigma(1)1}...A_{\sigma(n)n}\det(e_{\sigma(1)},...,e_{\sigma(n)}) = \sum_{\sigma}A_{\sigma(1)1}...A_{\sigma(n)n}sign(\sigma)\]
giving an explicit formula, assuming \(\det(e_1,...,e_n) = 1\). This explicit formula hence shows the determinant is unique.

For existence, take this formula as the definition (writing \(v_j = \sum_{i = 1}^nA_{ij}e_i\)). The formula is multi-linear: multiplying \(v_j\) by a scalar \(det(...)\) by that scalar. If \(v_j= v_j' + v_j''\), replace \(A_{ij}\) with \(A_{ij}' + A_{ij}''\), showing \(\det(...) = \det(...,v_j',...) + \det(...,v_j'',...)\).

Finally, if \(v_r = v_s\), for some \(r < s\), then for each \(\sigma\) there is \(\sigma'\) obtained from \(\sigma\) by interchanging \(r\) and \(s\). \(sign(\sigma) = sign(\sigma')\). But \(A_{\sigma(s)r}A_{\sigma{r}s} = A_{\sigma'(s)r}A_{\sigma'(r)s}\), because \(A_{rs} = A_{sr}\), consecutive terms cancel.
\end{proof}

\begin{definition}
For \(A \in M_{n \times n}(F)\), we define \(\det(A) = \det(v_1...,v_n)\) where \(v_1,...,v_n\) are the columns of \(A\).
\end{definition}

For small \(n\):
\begin{itemize}
    \item \(n = 1, A = (A_{11}), \det(A) = A_{11}\)
    \item \(n = 2\):
    \[A = \begin{pmatrix}A_{11} & A_{12} \\ A_{21} & A_{22}\end{pmatrix}\]
    \[\det(A) = A_{11}A_{22} - A_{21}A_{12}\]
    \item \(n = 3\):
    \[A = \begin{pmatrix} A_{11} & A_{12} & A_{13} \\
    A_{21} & A_{22} & A_{23} \\
    A_{31} & A_{32} & A_{33}\end{pmatrix}\]
    We list out the permutations of \(\{1, 2, 3\}\) and sum.
    In general, \(\{1,...,n\}\) has \(n!\) permutations, which grows \underline{very} quickly with \(n\), hence in most cases this method is just too complicated.
\end{itemize}

\begin{proposition}
If \(A \in M_{n \times n}(F)\) is upper triangular (or lower triangular), then \(\det(A) = A_{11}A_{22}...A_{nn}\)
\end{proposition}
\begin{proof}
\(A\) being upper triangular means \(A_{ij} = 0\) for \(i > j\), so in the complicated formula for the determinant, \(\sigma\) contributes only if \(\sigma(1) \geq 1, \sigma(2) \geq 2, \sigma(3) \geq 3,...\). This hence gives only the trivial permutation \(\{1, 2, 3,...\}\).
\end{proof}

\begin{theorem}
For every \(A \in M_{n \times n}(F)\), \(\det(A) = \det(A^T)\).
\end{theorem}
\begin{proof}
Follows from the explicit formula given in Theorem 1. Given \(\sigma\), we can consider the inverse permutation \(\tau = \sigma^{-1}\). Have \(sign(\sigma) = sign(\tau)\). So
\[\det(A) = \sum_{\sigma}sign(\sigma)A_{\sigma(1)1}...A_{\sigma(n)n} = \sum_{\sigma}sign(\tau)A^T_{\tau(1)1}...A^T_{\tau(n)n} = \det(A^T)\]
\end{proof}
\end{document}