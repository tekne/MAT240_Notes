\documentclass{article}
\title{MAT240 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{November 2 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\begin{document}
\maketitle

A few more words on the ranks of matrices: these are pretty easy to calculate, because of their invariance under row and column operators. This can be used to calculate the ranks of general linear maps \(T: V \to W\) where \(V, W\) are finite dimensional. We only need to introduce a basis, and then look at the rank of that transformation represented in that basis. That is, \[rank(T) = rank([T]^\gamma_\beta)\] where \(\beta\) is a basis of \(V\) and \(\gamma\) is a basis of \(W\).

Reason: the bases \(\beta, \gamma\) are isomorphisms \[\phi_\beta: V \to F^n, \phi_\gamma: W \to F^m \implies T = \phi_\gamma^{-1} \circ L_A \circ \phi_\beta\]\[ \implies rank(T) = rank(\phi_\gamma^{-1} \circ L_A \circ \phi_\beta) = rank(L_A) = rank(A)\] by definition. Thus, to calculate the rank of an abstract linear map, we must only introduce bases and calculate the rank of the corresponding matrix.

Matrix inversion: If \(A \in M_{n \times n}(F)\) is invertible, then \(Ax = b \iff x = bA^{-1}\). We work as follows: \((A|b) \to (I|c) \implies Ix = c\) We apply this observation to the \(j^{th}\) standard basis vector. \(Ax = e_{j} \implies (A|e_{j}) \to (I|c) \implies Ix = c\).

We now extend this to all the standard basis vectors, e.g. the identity matrix: \((A|e_1 ... e_n) = (A|I) \to (I|c_1,...,c_n) = (I,A^{-1})\).

Example: \(F = \mathbb{Z}_5\). Compute \(A^{-1}\) for
\[A = \begin{pmatrix}
1 & 1 & 1 \\
1 & 3 & 4 \\
1 & 4 & 1
\end{pmatrix}\]

We begin performing row operations on the augmented matrix and perform Gaussian elimination on the first column:
\[A = \left(\begin{array}{ccc|ccc}
1 & 1 & 1 & 1 & 0 & 0\\ 1 & 3 & 4 & 0 & 1 & 0\\ 1 & 4 & 1 & 0 & 0 & 1
\end{array}\right) \implies
\left(\begin{array}{ccc|ccc}
1 & 1 & 1 & 1 & 0 & 0\\ 0 & 2 & 3 & 4 & 1 & 0\\ 0 & 3 & 0 & 4 & 0 & 1
\end{array}\right)\]
We proceed to the second column
\[
\left(\begin{array}{ccc|ccc}
1 & 1 & 1 & 1 & 0 & 0\\ 0 & 1 & 4 & 2 & 3 & 0\\ 0 & 1 & 0 & 3 & 0 & 2
\end{array}\right) \implies
\left(\begin{array}{ccc|ccc}
1 & 1 & 1 & 1 & 0 & 0\\ 0 & 1 & 4 & 2 & 3 & 0\\ 0 & 0 & 1 & 1 & 2 & 2
\end{array}\right)
\]
Now we eliminate nonzero entries above the second and third columns:
\[
\left(\begin{array}{ccc|ccc}
1 & 1 & 1 & 1 & 0 & 0\\ 0 & 1 & 4 & 2 & 3 & 0\\ 0 & 1 & 0 & 3 & 0 & 2
\end{array}\right) \implies
\left(\begin{array}{ccc|ccc}
1 & 0 & 0 & 2 & 3 & 1\\ 0 & 1 & 0 & 3 & 0 & 2\\ 0 & 0 & 1 & 1 & 2 & 2
\end{array}\right) \implies 
A^{-1} = \begin{pmatrix}
2 & 3 & 1 \\
3 & 0 & 2 \\
1 & 2 & 2
\end{pmatrix}
\]
For \(2 \times 2\) matrices we have a formula:
\begin{theorem}
The matrix \(A = \begin{pmatrix} a & b \\ c & d \end{pmatrix}\) is invertible if and only if \(ad - bc = 0\) and in this case \[A^{-1} = \frac{1}{ad - bc}\begin{pmatrix}d & -b \\ -c & a\end{pmatrix}\] Note: \(\Delta = ad - bc\) is a special case of the \underline{determinant}of \(A\)
\end{theorem}

Example: \(F = \mathbb{C}\). \[A = \begin{pmatrix} i & 2 \\ 1 & 3i \end{pmatrix} \iff A^{-1} = -\frac{1}{5}\begin{pmatrix}3i & -2\\-1 & i\end{pmatrix}\]

\begin{proof}
If \(ad - bc \neq 0\), checking the formula works: \[\frac{1}{ad - bc}\begin{pmatrix}d & -b \\ -c & a\end{pmatrix}\begin{pmatrix}a & b \\ c & d\end{pmatrix} = \begin{pmatrix}1 & 0\\0 & 1\end{pmatrix}\]
For \(ad - bc = 0\), the matrix is not invertible because the columns are linearly dependent:
\[d\begin{pmatrix}a \\ c\end{pmatrix} - c\begin{pmatrix}b \\ d\end{pmatrix} = 0\] (if \(c\) or \(d \neq 0\), otherwise, we still have linear dependence)
\end{proof}

\section{General Theory of Linear Systems}

Of the form
\[A_{11}x_1 + ... + A_{1n}x_n = b_1, ..., A_{m1}x_1 + ... + A_{mn}x_n = b_m \iff Ax = b, A \in M_{m \times n}(F)\]
Terminology: If \(b = 0\), it's called \underline{homogeneous} (in general, called \underline{inhomogeneous}

\begin{itemize}

\item [Observation:] The solution set of the homogeneous equation \(Ax = 0\) is a subspace of \(F^n\) of dimension \(n - rank(A)\) (because solution set \(N(L_A\) has dimension \(nullity(A)\))
\item [Proposition:] Let \(s \in F^n\) be any solution of the inhomogeneous equation \(Ax = b\). Then the solution set of the whole equation is given by \(x = s + y\) where \(y\) is a solution of \(Ay = 0\).

The solution set is an "affine subspace" (e.g. a straight line that does not go through 0).

\begin{proof}
If \(Ay = 0\), \(A(s + y) = As + Ay = b + 0 = b\), implying \(s + y\) is a solution.
Conversely, if \(Ax = b\), \(A(x - s) = Ax - As = b - b = 0 = Ay\)
\end{proof}
\end{itemize}


\end{document}