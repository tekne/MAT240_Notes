\documentclass{article}
\title{MAT240 Notes: Theory of  Linear Systems}
\author{Jad Elkhaleq Ghalayini}
\date{November 21 2017}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{fact}{Fact}
\newtheorem*{note}{Note}
\begin{document}
\maketitle

The homogeneous equation \(Ax = 0\) has solution set \(N(L_A)\) and is a subspace of dimension \(n - rank(A)\) (for an \(m \times n\) matrix). The inhomogeneous equation \(Ax = b\) has all solutions given by one particular solution \(x\) plus any solution \(y\) (including 0) to the homogeneous equation. Note adding solutions \(x,y\) to the homogeneous equation gives another solution, as \(A(x + y) = Ax + Ay = 0\).

Example:
\[x_2 + x_3 = 2\]\[x_1 - 3x_3 + 2x_4  = 3\]\[2x_1 + 2x_2 - 4x_3 + 5x_4 = 11\]\[x_1 + x_2- 2x_3 + 2x_4 = 5\]
We write down the augmented matrix
\[(A|x) = \left(\begin{array}{cccc|c}
0 & 1 & 1 & 0 & 2\\
1 & 0 & -3 & 2 & 3\\
2 & 2 & -4 & 5 & 11\\
1 & 1 & -2 & 2 & 5
\end{array}\right) \implies
\left(\begin{array}{cccc|c}
1 & 0 & -3 & 2 & 3\\
0 & 1 & 1 & 0 & 2\\
2 & 2 & -4 & 5 & 11\\
1 & 1 & -2 & 2 & 5
\end{array}\right) \implies\]
\[\left(\begin{array}{cccc|c}
1 & 0 & -3 & 2 & 3\\
0 & 1 & 1 & 0 & 2\\
0 & 2 & 2 & 1 & 5\\
0 & 1 & 1 & 0 & 2
\end{array}\right)
\implies \left(\begin{array}{cccc|c}
1 & 0 & -3 & 2 & 3\\
0 & 1 & 1 & 0 & 2\\
0 & 0 & 0 & 1 & 1\\
0 & 0 & 0 & 0 & 0
\end{array}\right)
\implies \left(\begin{array}{cccc|c}
1 & 0 & -3 & 0 & 1\\
0 & 1 & 1 & 0 & 2\\
0 & 0 & 0 & 1 & 1\\
0 & 0 & 0 & 0 & 0
\end{array}\right)\]
We can now directly see \(rank(A) = 3 \implies nullity(A) = 1\), e.g. the homogeneous solution has a one-dimensional solution space. We can easily find this by just replacing the rightmost column with all zeroes to get the system:
\[y_1 - 3y_3 = 0, y_2 + y_3 = 0, y_4 = 0, 0 = 0\]
This gives solution vector \(t(3 \ -1 \ 1 \ 0)^T\), for any \(t \in F\) (since we have a one dimensional solution space).

We now solve the inhomogeneous equation: \[x_4 = 1, x_2 + x_3 = 2, x_1 - 3x_3 = 1, 0 = 0\] We set \(x_3 = 0\) to determine \(x_1, x_2, x_4\) and obtain the general solution set
\[\left\{\left.\begin{pmatrix}1 \\ 2 \\ 0 \\ 1\end{pmatrix} + t\begin{pmatrix}3 \\ -1 \\ 1 \\ 0\end{pmatrix}\right| t \in F\right\}\]
Note this represents the points on a line in 4D space.

Given matrix \(A \in M_{n \times n}(F)\), what is the simplest form we can take it to using only row operations? This is called Reduced Row Echelon Form (RREF).

We start with row reduced form: where each row starts with a \(1\), and each row's first element is to the right of the first elements of previous rows. Clearing out all nonzero entries above each 1, we reach RREF, the simplest form we can reach.

\begin{definition}
A matrix \(A \in M_{m \times n}(F)\) is in \underline{reduced row echelon form} if it has the following properties
\begin{itemize}
    \item The non-zero rows are above the zero rows
    \item The first non zero entry of any row is a 1, and is the only non-zero entry in its column
    \item The first nonzero entry of any row appears to the right of the first nonzero entry of previous rows
\end{itemize}
\end{definition}
\begin{theorem}
Every \(A \in M_{n \times n}(F)\) can be brought to reduced row echelon form using row operations
\end{theorem}
\begin{definition}
Suppose \(A\) is in reduced row echelon form. The first nonzero entry of any row is called a \underline{pivotal} entry. Its column is called a \underline{pivotal} column
\end{definition}
Note each of these columns are like standard basis vectors: only a single one among many zeroes.

These are important in a number of ways: we can, for example, immediately read off the row of the matrix: the number of pivotal columns, which are clearly linearly independent, but can zero all other nonzero elements through column operations (which do not change rank). Hence, by the Rank Nullity theorem, we can immediately see the nullity of \(A\) is the number of non-zero columns.

\subsection{Homogeneous Equations} 

If \(A\) is in reduced row echelon form, the solution of \(Ax = 0\) is determined by taking arbitrary values for \(x_i\) if column \(i\) is non-pivotal, the values of \(x_i\) for pivotal columns are determined.

Example:
\[A = \begin{pmatrix} 1 & 0 & -3 & 0 & 4 \\ 0 & 1 & 1 & 0 & 1 \\ 0 & 0 & 0 & 1 & -2 \\ 0 & 0 & 0 & 0 & 0\end{pmatrix}\]
We select arbitrary values of \(x_3, x_5\), and \(x_1, x_2, x_4\) are determined by the equations. We can see this looking at the system:
\[x_1 - 3x_3 + 4x_5 = 0, x_2 + x_3 + x_5 = 0, x_4 - 2x_5 = 0, 0 = 0\]
We can also see we get a basis, which is two dimensional (as we have two values to choose, giving two linearly independent vector)
For example, if we choose \(x_3 = 1, x_5 = 0\), we obtain
\(x_1 = 3, x_2 = -1, x_4 = 0\), and if we choose \(x_3 = 0, x_5 = 1\), we obtain \(x_1 = -4, x_2 = -1, x_4 = 2\). This gives us two linearly independent basis vectors for the nullspace.

Consider the inhomogeneous equation \(Ax = b\). First, we put the augmented matrix \((A|b)\) into reduced row echelon form, which also puts \(A\) into reduced row echelon form. We can now see directly if a solution exists or not: if the matrix for \(A\) has the last \(n\) rows be zeroes, then the corresponding elements in \(b\) must also be zero, else we have a contradiction So \(Ax = b\) has a solution if and only if the zero rows of \(rref(A)\) are also zero rows of \(rref((A|B))\). Suppose this is the case. Then, a solution to the homogeneous equation is obtained by setting the pivotal columns to arbitrary values, and a specific solution to the non-homogeneous equation is given by setting the non-pivotal columns to arbitrary values (say, zero).

\subsection{Determining the range of a linear transformation}
Let \(A \in M_{m \times n}(F)\). \(R(L_A) \subseteq F^n\). We know \(R(L_A)\) is a subspace spanned by the columns of \(A\). So we get a basis of \(R(L_A)\) by taking any maximal  linearly independent subset of the set of columns.

We can use row operations for this (which may be somewhat confusing, because row operations change the range). Let \(v_1,...,v_n\) be the columns of \(A\), and let \(v_1',...,v_n'\) be the columns of \(A'\) which is obtained from the columns of \(A\) by some row operations. Then \[\sum_{k = 1}^na_kv_k = 0 \iff \sum_{k = 1}^na_kv'_k\] (with the same coefficients \(a_1,...,a_n\)). Conclusion: if \(A'\) can be obtained from \(A\) by row operations and \(A'\) has reduced row echelon form, then the indices of the pivotal columns correspond to the indices of vectors which form a maximal linearly independent subset of the set of columns.
\end{document}